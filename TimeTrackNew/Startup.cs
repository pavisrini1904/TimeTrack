﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TimeTrackNew.Startup))]
namespace TimeTrackNew
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
