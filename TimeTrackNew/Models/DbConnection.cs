﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace TimeTrackNew.Models
{
    public class DbConnection
    {
        SqlConnection con;
        public DbConnection()
        {
            con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["connectionString"].ConnectionString);
        }
        public SqlConnection connection()
        {
            if (con.State == ConnectionState.Open || con.State == ConnectionState.Broken || con.State == ConnectionState.Closed)
            {
                con.Close();
            }
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            return con;
        }
    }
}