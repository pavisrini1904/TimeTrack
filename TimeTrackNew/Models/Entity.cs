﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TimeTrackNew.Models
{
    public class Entity
    {
        public string strSGID { get; set; }
        public string strFlag { get; set; }
        public string strimagename { get; set; }
        public string strimagepath { get; set; }
    }
    public class Month
    {
        public string strSGID { get; set; }
        public string strFlag { get; set; }
        public string strDate { get; set; }
        public string strPreNext { get; set; }
        public string strempname { get; set; }
    }
    class TaskDetails
    {
        public string strTeam { get; set; }

        public string strPROJECT { get; set; }

        public string strTASKTYPE { get; set; }

        public string strTASK { get; set; }

        public string strSTARTTIME { get; set; }

        public string strENDTIME { get; set; }

        public string PROJECTNAME { get; set; }

        public string ROWID { get; set; }

        public string WORKEDHrS { get; set; }


        public string DOJ { get; set; }


        public string NAME { get; set; }


        public string SGID { get; set; }


        public string strLopdays { get; set; }
        public string strFlag { get; set; }
        public string strDate { get; set; }
        public string strPreNext { get; set; }
        public string strempname { get; set; }
    }
}