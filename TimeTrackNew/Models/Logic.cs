﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using TimeTrackNew.Models;

namespace TimeTrackNew.Models
{
    class Logic
    {
        DbConnection Dbconnection = new DbConnection();
        SqlConnection Conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ToString());
        StoreProcedure SP = new StoreProcedure();
        SqlDataAdapter Adapter = new SqlDataAdapter();
        SqlCommand Cmd = new SqlCommand();
        Entity userEntity = new Entity();
        public DataSet GetEmployeeDetails(string strSGID, string strPassword)
        {
            Conn.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = Dbconnection.connection();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = SP.EmpGetdetailsLoad;
            cmd.Parameters.AddWithValue("@SGID", strSGID);
            cmd.ExecuteNonQuery();
            Conn.Close();
            SqlConnection.ClearPool(Conn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet Ds = new DataSet();
            da.Fill(Ds);
            if (Ds.Tables.Count > 0)
            {
                return Ds;
            }
            return null;
        }
        public DataSet GetProfileDetail(string strSGID, string strDate, string strPreNext, string strFlag)
        {
            Conn.Open();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = Dbconnection.connection();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = SP.Getmonthload;
            cmd.Parameters.AddWithValue("@SGID", strSGID);
            cmd.Parameters.AddWithValue("@ImageName", strDate);
            cmd.Parameters.AddWithValue("@ImagePath", strPreNext);
            cmd.Parameters.AddWithValue("@Flag", strFlag);
            cmd.ExecuteNonQuery();
            Conn.Close();
            SqlConnection.ClearPool(Conn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet Ds = new DataSet();
            da.Fill(Ds);
            if (Ds.Tables.Count > 0)
            {
                return Ds;
            }
            return null;
        }

        internal DataSet Getmonthload(Entity UserEntity)
        {
            throw new NotImplementedException();
        }
    }
}