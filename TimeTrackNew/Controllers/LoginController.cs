﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TimeTrackNew.Models;

namespace TimeTrackNew.Controllers
{
    public class LoginController : Controller
    {

        Logic UserLogic = new Logic();
        Entity UserEntity = new Entity();
        // GET: Login
        public ActionResult Login()
        {
            return View();
        }
        public ActionResult Dummylogin()
        {
            return View();
        }

        [HttpPost]
        public ActionResult DummyLogin(FormCollection FormCollection)
        {
            
            try
            {
                string strUsername = Request["txtSGID"].ToString();
                string strPassword = strUsername;
                if (!string.IsNullOrEmpty(strUsername))
                {
                    
                    System.Data.DataSet ds = UserLogic.GetEmployeeDetails(strPassword, "");
                    if (ds.Tables[0].Rows.Count > 0)
                    {

                        ViewBag.EmployeeDetails = ds.Tables[0];
                        Session["EmpName"] = ds.Tables[0].Rows[0]["TSR_Name"];
                        Session["EmpSGID"] = ds.Tables[0].Rows[0]["TSR_SGID"];
                        Session["IMAGEPATH"] = ds.Tables[0].Rows[0]["ImagePath"];
                        Session["TlFlag"] = Convert.ToString(ds.Tables[0].Rows[0]["TST_TeamLeaderFlag"]);
                        Session["TeamName"] = ds.Tables[0].Rows[0]["TST_Team"];
                        Session["LevelFlag"] = ds.Tables[0].Rows[0]["TlFlag"];
                        Session["ManualAccess"] = ds.Tables[0].Rows[0]["TSR_AccesCard_Flag"];
                        Session["ShftMenu"] = ds.Tables[0].Rows[0]["ShftMenu"];
                        Session["ShiftTeam"] = ds.Tables[0].Rows[0]["ShiftTeam"];
                        Session["EMPCATEGORY"] = ds.Tables[0].Rows[0]["EMPCATEGORY"];

                        return RedirectToAction("Index", "Home", new { Area = "" });
                    }
                    else
                    {

                        return RedirectToAction("Index", "Login");
                    }
                }

            }
            catch (Exception e)
            {
                throw;
            }
            return View();
        }
    }
}